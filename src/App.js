import React from "react";
import './App.css';
import Header from './components/Header';
import SherchPanel from './components/SherchPanel';

function App() {
  return (
    <div className="App">
      <Header />
      <SherchPanel />
    </div>
  );
}

export default App;
